# Mastrap

## Présentation

## Dépendances

### Prérequis

- NodeJS
- NPM
- Gulp CLI

## Contribuer

Afin de contribuer correctement au code, veuillez définir vos informations de profil :

```
git config user.name "[NOM] [PRENOM]"
git config user.email "email@viacesi.fr"
```

### Installation de NodeJS / NPM

Vous pouvez installer NodeJS facilement depuis leurs site internet : https://nodejs.org/en/download/package-manager/

### Installation de Gulp CLI

Gulp CLI s'installe en tant que dépendance globale de votre système via la commande :

```bash
# ATTENTION : Vous devez être administrateur de votre système pour utiliser cette commande
npm install -g gulp-cli@2.0.0
```

## Compilation

### Installation des packages

Pour que Mastrap fonctionne correctement, vous avez besoin d'installer les packages du projet, utiliser cette commande à la racine du projet :

```bash
npm install
```

## Utilisation

### Développement

En mode de développement, Gulp permet de compiler en temps réel les sources SCSS, pour  cela vous devez utiliser la commande : 

```bash
# Démarrage du processus de compilation du CSS en temps réel
gulp watch
```

### Production

Pour la production et la génération de Tags, il est nécessaire de générer le dossier `dist` via le processus de construction des fichiers, pour cela vous devez utiliser les commandes suivantes : 

 ```bash
# Compilation du CSS
gulp build-theme

# Compilation du JS
gulp build-js
 ```

